function getRemainingPathPoints(vehicle, path) {
  let positionIndex = vehicle.pathPositionIndex
  if(!positionIndex) {
    let minDistance = calcDistance(vehicle.position, path[0])
    path.slice(1).forEach((point, i) => {
      const distance = calcDistance(vehicle.position, point)
      if(!positionIndex || distance < minDistance) {
        positionIndex = i+1
        minDistance = distance
      }
    })
  }
  return path.slice(positionIndex)
}

function calcDistance(point1, point2) {
  var p1x = +point1.x,
      p2x = +point2.x,
      p1z = +point1.z,
      p2z = +point2.z;
  return Math.sqrt(Math.pow(p1x - p2x, 2) + Math.pow(p1z - p2z, 2));
}

class Simulation {
  constructor() {
    this._initialized = false;
    this._data = null;
    this._currentState = null;
    this._running = false;
    this._step = 0;
    this._speed = 500;
    this._nextIntervalId = null;
    this._callback = null;

    this.init = this.init.bind(this)
    this.setSpeed = this.setSpeed.bind(this)
    this.nextStep = this.nextStep.bind(this)
    this.run = this.run.bind(this)
    this.pause = this.pause.bind(this)
    this.reset = this.reset.bind(this)
  }

  init(vehicles, stations, suggestions, params, speed=500) {
    if(this._running || this._initialized && this._data != this._currentState) {
      this.log('Stop simulation and reset to be able to reinitialize')
      return
    }
    if(!vehicles || !stations) {
      this.log('Insufficient init parameters')
      return
    }
    this._data = {
      vehicles: vehicles,
      vehiclesParked: [],
      stations: stations,
      suggestions: suggestions,
      params: params
    }
    this._currentState = JSON.parse(JSON.stringify(this._data))
    this._speed = speed
    this._step = 0
    
    this._initialized = true
    this.log('Ready')
  }

  bindCallback(fn) {
    this._callback = fn
  }

  setSpeed(interval) {
    this._speed = interval;
  }

  nextStep() {
    if(this._initialized) {
      if(this._currentState.vehicles.length == 0) {
        this.log('SIMULATION ENDED. You have to RESET.');
        this.pause()
        return
      }
  
      const step = this._step
      let vehiclesParked = this._currentState.vehiclesParked
      let stations = this._currentState.stations
      const M = this._currentState.params.m
      const suggestions = this._currentState.suggestions
  
      this._currentState.vehicles.forEach((vehicle, i, vehicles) => {
        let vehiclePath = vehicle.path
        if(vehiclePath.length == 0) {
          vehiclesParked.push({
            'position': vehicle.position,
            'dataset': 'Parked Vehicle'
          });
          vehicles.splice(i, 1)
          return
        }
        const pathPoints = getRemainingPathPoints(vehicle, vehiclePath)

        if(!pathPoints || pathPoints.length == 0) {
          vehiclesParked.push({
            'position': vehicle.position,
            'dataset': 'Parked Vehicle'
          });
          vehicles.splice(i, 1)
        } else {
          vehicles[i].position = pathPoints[0] || vehicles[i].position
          vehiclePath.length = 0
          pathPoints.slice(1).forEach((pathPoint) => {
            vehiclePath.push(pathPoint)
          })
        }
      })
      this._currentState.vehiclesParked = vehiclesParked

      this._currentState.stations.forEach((station, i) => {
        var suggested = suggestions.filter(sugg => {
          return sugg && sugg.index == i
          && sugg.offIndex > step - M
          && sugg.offIndex <= step;
        }).length;
        stations[i].n_load = station.n_hereNow + suggested
      })
      this._currentState.stations = stations
  
      if(this._callback) this._callback(this._currentState)
      this._step++
      this.log('Step #' + this._step)
    } else {
      this.log('INIT SIMULATION before running it')
      this._running = false
    }
  }

  run() {
    if(this._nextIntervalId) this.pause()
    this._nextIntervalId = setInterval(this.nextStep, this._speed)
    this._running = true
  }

  pause() {
    clearInterval(this._nextIntervalId)
    this._running = false
  }

  reset() {
    if(this._running) this.pause()
    this._currentState = JSON.parse(JSON.stringify(this._data))
    this._step = 0
    if(this._callback) this._callback(this._data)
  }

  log(msg) {
    console.log(`Simulation | ${msg}`)
  }
};