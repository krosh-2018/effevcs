const EventEmitter = require('events').EventEmitter;
const helpers = require('./helpers')

class Recommender extends EventEmitter {
  constructor(vehicles, stations, params) {
    super();
    this.vehicles = vehicles
    this.stations = stations
    this.maxVisitors = this.calcMaxVisitors(stations)
    
    this.params = params

    this._radius = 200
    this._suggestions = []
    this._running = false
    this._suggMap = {}

    this.calcMaxVisitors = this.calcMaxVisitors.bind(this)
    this.calcSuggestion = this.calcSuggestion.bind(this)
    this.calcSuggestions = this.calcSuggestions.bind(this)
    this.setSuggestions = this.setSuggestions.bind(this)
  }

  calcMaxVisitors(stations) {
    if(!stations) return;

    return stations.reduce((res, item) => {
      return ( res === undefined || item.n_hereNow > res )
        ? item.n_hereNow
        : res
    }, [])
  }

  calcSuggestion(vehicle) {
    if(!vehicle || vehicle.path.length == 0) return;

    const pathPoints = helpers.getRemainingPathPoints(vehicle, vehicle.path)
    if(!pathPoints) return;

    const stations = this.stations
    let betterEVCS = {}
    pathPoints.forEach((point, i) => {
      let maxScore = 0
      let maxIndex = -1
  
      const maxDistance = stations.reduce((res, station) => {
        const distance = helpers.calcDistance(station.position, point)
        return (res === undefined || distance > res)
          ? distance
          : res
      }, [])
  
      const _suggMap = this._suggMap
      const params = this.params
      stations.forEach(function(station, j) {
        const distance = helpers.calcDistance(station.position, point);
        const suggested = _suggMap[`${i}.${j}`] || 0;
  
        let score = 0;
        const load = station.n_hereNow + suggested;
        if(load < params.cap) {
          const score1 = load;
          const score2 = distance/maxDistance;
          score = 1/(params.k1*score1 + params.k2*score2);
        }
  
        if(score > maxScore) {
          maxScore = score;
          maxIndex = j;
        }
      })
  
      if(maxIndex == -1) return;
  
      if(maxIndex in betterEVCS) {
        betterEVCS[maxIndex].score += maxScore;
      } else {
        betterEVCS[maxIndex] = {};
        betterEVCS[maxIndex].score = maxScore;
        betterEVCS[maxIndex].offIndex = i;
      }
    })

    let maxScore = 0
    let maxIndex = -1
    let offIndex = 0


    if(Object.keys(betterEVCS).length > 0) {
      for(let index in betterEVCS) {
        if(betterEVCS[index].score > maxScore || maxScore == -1) {
          maxScore = betterEVCS[index].score
          maxIndex = index
          offIndex = betterEVCS[index].offIndex
        }
      }
    }

    let suggestion = null;
    if(maxIndex != -1) {
      suggestion = {
        position: this.stations[maxIndex].position,
        dataset: 'PATH-SUGGESTION',
        n_hereNow: this.stations[maxIndex].n_hereNow,
        index: +maxIndex,
        offIndex: offIndex,
        vehicleId: vehicle.id
      };
      for(let k = -this.params.m + 1; k < this.params.m; k++) {
        this._suggMap[`${offIndex + k}.${maxIndex}`] =
          this._suggMap[`${offIndex + k}.${maxIndex}`] != null
            ? this._suggMap[`${offIndex + k}.${maxIndex}`] + 1
            : 1
      }
    }

    return suggestion
  }

  calcSuggestions() {
    this._suggestions.length = 0

    let suggestions = this._suggestions
    const calc = (i) => {
      
      if(!this._running) {
        this.emit('stop')
        return;
      }

      if(i == this.vehicles.length) {
        this.emit('done', this._suggestions)
        return
      }

      suggestions.push(this.calcSuggestion(this.vehicles[i], suggestions))
      this.emit('progress', { step: i+1, size: this.vehicles.length })

      setImmediate(() => calc(i+1))
    }
    calc(0)
    this._running = true
    this.emit('start')
  }

  getSuggestions() {
    return this._suggestions
  }

  setSuggestions(suggestions) {
    this._suggestions = suggestions;
  }

  start() {
    this._running = true;
  }
  stop() {
    this._running = false;
  }

  calcEffectiveness() {
    if(!this._suggestions || this._suggestions.length == 0
      || !this.params.m) return;
  
    let maxHereNow = -1;
    this.stations.forEach(function(station) {
      if(station.n_hereNow > maxHereNow)
        maxHereNow = station.n_hereNow;
    });

    let vehicles = JSON.parse(JSON.stringify(this.vehicles));
  
    const effs = [];
    let n = 0;
    while(vehicles.length > 0) {
      const loads = Array.apply(null, new Array(this.stations.length))
        .map(Number.prototype.valueOf,1); // array of ones
  
      this.stations.forEach((station, j, _) => {
        const suggestedHereNow = this._suggestions.filter(sugg => {
          return sugg && sugg['index'] == j
          && sugg['offIndex'] > n - this.params.m
          && sugg['offIndex'] < n + this.params.m;
        }).length;
        loads[j] = station.n_hereNow + suggestedHereNow;
      });
  
      const avg = loads.reduce(function(acc, val) { return acc + val; }) / loads.length;
      const standartDeviation = Math.sqrt(
        loads.reduce(function(acc, val) { return acc + Math.pow(val - avg, 2); }) / loads.length);
      const effectiveness = standartDeviation / avg; // Coef. of Variation (if < 0.33 then uniform)
      effs.push(effectiveness);
  
      n++;
      vehicles = vehicles.filter(vehicle => vehicle.path.length > n);
    }
  
    // Get results and print it
    const sumEff = effs.reduce(function(acc, val) { return acc + val; }, 0);
    const avgEff = sumEff / effs.length;
    return avgEff;
  }
}

module.exports = Recommender