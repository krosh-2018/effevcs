const calcDistance = (point1, point2) => {
  var p1x = +point1.x,
      p2x = +point2.x,
      p1z = +point1.z,
      p2z = +point2.z;
  return Math.sqrt(Math.pow(p1x - p2x, 2) + Math.pow(p1z - p2z, 2));
}

const getRemainingPathPoints = (vehicle, path) => {
  let positionIndex = vehicle.pathPositionIndex
  if(!positionIndex) {
    let minDistance = calcDistance(vehicle.position, path[0])
    path.slice(1).forEach((point, i) => {
      const distance = calcDistance(vehicle.position, point)
      if(!positionIndex || distance < minDistance) {
        positionIndex = i+1
        minDistance = distance
      }
    })
  }
  return path.slice(positionIndex)
}

module.exports = {
  calcDistance, getRemainingPathPoints
}