const fs = require('fs');
const path = require('path');
const rfs = require('rotating-file-stream');
const morgan = require('morgan');

const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const http = require('http').Server(app);
const io = require('socket.io').listen(http);

const bodyParser = require('body-parser');

const Recommender = require('./recommender')

let producerId = null;

const PORT = process.env.PORT || 3000


// --- Set up Logging ---
const logDirectory = path.join(__dirname, 'logs')
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory)

const accessLogStream = rfs('access.log', {
  interval: '1d', // rotate daily
  path: logDirectory
})
// log only 4xx and 5xx responses to console
app.use(morgan('dev', { skip: (_, res) => res.statusCode < 400 }))
// log all requests to access.log
app.use(morgan('combined', {stream: accessLogStream}))


app.use(bodyParser.urlencoded({limit: '30mb', extended: true }))
app.use(bodyParser.json({ limit: '30mb', extended: true }))
app.use(express.static('static'))

app.get('/', (_, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.get('/scripts/simulation.js', (_, res) => {
  res.sendFile(__dirname + '/simulation.js')
})

const extendTimeoutMiddleware = (req, res, next) => {
  const space = ' ';
  let isFinished = false;
  let isDataSent = false;

  res.once('finish', () => {
    isFinished = true;
  });

  res.once('end', () => {
    isFinished = true;
  });

  res.once('close', () => {
    isFinished = true;
  });

  res.on('data', (data) => {
    // Look for something other than our blank space to indicate that real
    // data is now being sent back to the client.
    if (data !== space) {
      isDataSent = true;
    }
  });

  const waitAndSend = () => {
    setTimeout(() => {
      // If the response hasn't finished and hasn't sent any data back....
      if (!isFinished && !isDataSent) {
        // Need to write the status code/headers if they haven't been sent yet.
        if (!res.headersSent) {
          res.writeHead(202);
        }

        res.write(space);

        // Wait another 15 seconds
        waitAndSend();
      }
    }, 15000);
  };

  waitAndSend();
  next();
}

app.post('/calc_suggestions', extendTimeoutMiddleware, (req, res) => {
  if(!req.body || !req.body.vehicles || !req.body.stations
  || !req.body.k1 || !req.body.k2 || !req.body.m || !req.body.cap) {
    return res.send(400)
  }

  const socketId = req.body.socketId
  const recommender = new Recommender(
    req.body.vehicles,
    req.body.stations,
    { k1: req.body.k1, k2: req.body.k2, m: req.body.m, cap: req.body.cap },
    io, socketId
  )
  recommender.on('start', () => { 
    res.send({
      status: 'start',
      data: null
    })
  })
  recommender.on('progress', (data) => {
    if(io.sockets.connected[socketId]) {
      io.sockets.connected[socketId].emit('progress', {
        step: data.step,
        size: data.size
      })
    } else {
      recommender.stop();
    }
  })
  recommender.on('done', (suggestions) => {
    if(io.sockets.connected[socketId]) {
      io.sockets.connected[socketId].emit('done', suggestions)
    } else {
      recommender.stop();
    }
  })
  recommender.on('stop', () => {})
  
  recommender.start()
  recommender.calcSuggestions()
})

app.post('/calc_effectiveness', extendTimeoutMiddleware, (req, res) => {
  if(!req.body || !req.body.vehicles || !req.body.stations || !req.body.suggestions
    || !req.body.m) {
    return res.send(400)
  }

  const recommender = new Recommender(
    req.body.vehicles,
    req.body.stations,
    { m: req.body.m }, null, null
  )
  recommender.setSuggestions(req.body.suggestions)
  const effectiveness = recommender.calcEffectiveness()
  res.send({ status: 'success', data: effectiveness })
})

io.on('connection', function(socket) {
  console.log('a user connected', socket.id);

  socket.on('disconnect', function() {
    console.log(`user ${socket.id} disconnected`);
  });

  socket.on('frame', function() {
    producerId = socket.id;
  });

  socket.on('vehicles', function(msg) {
    try {
      const data = JSON.parse(msg);
      io.emit('vehicles_update', data);
    } catch(e) {
      console.log('Invalid json');
    }
  });

  socket.on('evcs', function(msg) {
    try {
      const data = JSON.parse(msg);
      io.emit('evcs_update', data);
    } catch(e) {
      console.log('Invalid json');
    }
  });
  
});

http.listen(PORT, function(){
  console.log(`listening on *:${PORT}`);
});