using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Threading.Tasks;

using ICities;
using UnityEngine;
using ColossalFramework;
using ColossalFramework.Plugins;

using Socket.IO.NET35;


namespace EffectiveEVCS
{
    public class CSRealtime : ThreadingExtensionBase
    {
        private int m_frameCount = 0;
        private Socket.IO.NET35.Socket socket;
        private JavaScriptSerializer jss;

        //Thread: Main
        public override void OnCreated(IThreading threading)
        {
            DebugOutputPanel.AddMessage(PluginManager.MessageType.Message, "CSRealtime created");
            jss = new JavaScriptSerializer();
            jss.MaxJsonLength = Int32.MaxValue;
            socket = IO.Socket("http://127.0.0.1:3000");
        }
        //Thread: Main
        public override void OnReleased()
        {

        }

        override public void OnAfterSimulationFrame()
        {
            m_frameCount++;
            if (m_frameCount >= 300) // every minute (3600), every five seconds (300)
            {
                socket.Emit("frame");
                m_frameCount = 0;

                if(!socket.Connected)
                {
                    socket.Connect();
                }

                Task.Factory.StartNew(() => sendEVCSData());

                Task.Factory.StartNew(() => sendVehiclesData());
            }
        }

        private void sendEVCSData()
        {
            List<EVCSData> evcsData = GetEVCSData();
            string jsonEVCS = jss.Serialize(evcsData.ToArray());
            socket.Emit("evcs", jsonEVCS);
        }

        private void sendVehiclesData()
        {
            List<VehicleData> vehiclesData = GetVehiclesData();
            string jsonVehicles = jss.Serialize(vehiclesData.ToArray());
            socket.Emit("vehicles", jsonVehicles);
        }
        
        private List<VehicleData> GetVehiclesData()
        {
            List<VehicleData> vehiclesData = new List<VehicleData>();

            var vehicleManager = Singleton<VehicleManager>.instance;
            var vehicles = vehicleManager.m_vehicles.m_buffer;
            for (int i = 0; i < vehicles.Length; i++)
            {
                Vehicle vehicle = vehicles[i];
                bool isValid = vehicle.m_flags != 0;
                bool isDummy = (vehicle.m_flags & Vehicle.Flags.DummyTraffic) == Vehicle.Flags.DummyTraffic;
                bool isFlying = (vehicle.m_flags & Vehicle.Flags.Flying) == Vehicle.Flags.Flying;
                //bool isFloating = (vehicle.m_flags2 & Vehicle.Flags2.Floating) == Vehicle.Flags2.Floating; // water vehicles
                bool isFloating = vehicle.m_flags2 != (Vehicle.Flags2)0;
                bool isBicycle = vehicle.Info.m_vehicleType == VehicleInfo.VehicleType.Bicycle;
                bool isMetro = vehicle.Info.m_vehicleType == VehicleInfo.VehicleType.Metro;
                bool isTrain = vehicle.Info.m_vehicleType == VehicleInfo.VehicleType.Train;
                if (isValid && !isDummy && !isFlying && !isFloating && !isBicycle && !isMetro && !isTrain)
                {
                    Coords pos = new Coords();
                    pos.x = (vehicle.m_segment.a.x + vehicle.m_segment.b.x) / 2;
                    pos.y = (vehicle.m_segment.a.y + vehicle.m_segment.b.y) / 2;
                    pos.z = (vehicle.m_segment.a.z + vehicle.m_segment.b.z) / 2;

                    vehiclesData.Add(new VehicleData()
                    {
                        id = i,
                        pathId = vehicle.m_path,
                        pathPositionIndex = vehicle.m_pathPositionIndex,
                        path = GatherPathPoints(vehicle.m_path),
                        position = pos
                    });
                }
            }
            return vehiclesData;
        }

        private List<EVCSData> GetEVCSData()
        {
            String targetName = "530187613.SHELL gas station ploppable_Data";
            List<EVCSData> evcsData = new List<EVCSData>();

            var buildingManager = Singleton<BuildingManager>.instance;
            var buildings = buildingManager.m_buildings.m_buffer;
            for (int i = 0; i < buildings.Length; i++)
            {
                Building building = buildings[i];
                if (building.m_flags == Building.Flags.None) { continue; }
                Boolean isActive = (building.m_flags & Building.Flags.Active) == Building.Flags.Active;
                String name = building.Info.name;
                if (isActive && name == targetName)
                {
                    Coords pos = new Coords();
                    pos.x = building.m_position.x;
                    pos.y = building.m_position.y;
                    pos.z = building.m_position.z;
                    evcsData.Add(new EVCSData()
                    {
                        position = pos,
                        n_hereNow = building.m_citizenCount
                    });
                }
            }
            return evcsData;
        }

        private Coords[] GatherPathPoints(uint pathID)
        {
            List<Coords> pathPoints = new List<Coords>();

            PathUnit[] paths = Singleton<PathManager>.instance.m_pathUnits.m_buffer;
            NetLane[] lanes = Singleton<NetManager>.instance.m_lanes.m_buffer;
            List<PathUnit.Position> positions = new List<PathUnit.Position>();
            while (true)
            {
                for (int i = 0; i < paths[pathID].m_positionCount; i++)
                {
                    PathUnit.Position p = paths[pathID].GetPosition(i);

                    //Exclude invalid lanes
                    if (PathManager.GetLaneID(p) != 0)
                    {
                        positions.Add(p);
                    }

                }

                if (paths[pathID].m_nextPathUnit == 0)
                {
                    //Log.debug("Done");
                    break;
                }

                pathID = paths[pathID].m_nextPathUnit;
            }
            PathUnit.Position[] positionsArray = positions.ToArray();
            for (int i = 0; i < positionsArray.Length; i++)
            {
                Vector3 pv, dir;
                uint laneId = PathManager.GetLaneID(positionsArray[i]);
                lanes[laneId].CalculatePositionAndDirection(positionsArray[i].m_offset / 255.0f, out pv, out dir);
                Coords pos = new Coords();
                pos.x = pv.x; pos.y = pv.y; pos.z = pv.z;
                pathPoints.Add(pos);
            }
            return pathPoints.ToArray();
        }
    }
}
