﻿using System;

namespace EffectiveEVCS
{
    public struct Coords
    {
        public float x;
        public float y;
        public float z;
    }

    public class VehicleData
    {
        public int id { get; set; }
        public uint pathId { get; set; }
        public Coords[] path { get; set; }
        public Byte pathPositionIndex { get; set; }
        public Coords position { get; set; }
    }

    public class EVCSData
    {
        public Coords position { get; set; }
        public Byte n_hereNow { get; set; }
    }
}
