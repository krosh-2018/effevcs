﻿using ICities;

namespace EffectiveEVCS
{
    public class UserModInfo: IUserMod
    {
        public string Name 
        {
            get { return "Effective_EVCS"; }
        }

        public string Description 
        {
            get { return "Electric Vehicle Charging Station Network"; }
			
        }
    }
}
